<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('fname')->nullable()->default(null);
            $table->string('lname')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('address')->nullable()->default(null);
            $table->string('city')->nullable()->default(null);
            $table->string('country')->nullable()->default(null);
            $table->string('country_code')->nullable()->default(null);
            $table->string('postal_code')->nullable()->default(null);
            $table->string('state')->nullable()->default(null);
            $table->enum('status',['YES','NO'])->default('NO')->nullable()->default(null);
            $table->date('join_date')->nullable()->default(null);
            $table->dateTime('last_login')->nullable()->default(null);
            $table->string('ip_address')->nullable()->default(null);
            $table->unsignedInteger('refer_user_id')->nullable()->default(null);
            $table->float('total_cash',13,2)->nullable()->default(null);
            $table->float('cash_available',13,2)->nullable()->default(null);
            $table->float('cash_used',13,2)->nullable()->default(null);
            $table->float('cash_expire',13,2)->nullable()->default(null);
            $table->integer('login')->nullable()->default(0);
            $table->integer('register')->nullable()->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
