<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_store', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('session_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('cat_id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('color_id')->default(0);
            $table->unsignedInteger('size_id')->default(0);
            $table->unsignedInteger('qty');
            $table->float('price',13,2);
            $table->string('name',250);
            $table->date('sysdate');
            $table->string('time',250);
            $table->integer('status');
            $table->float('o_price',13,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_store');
    }
}
