<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductShadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_shades', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('product_id');
            $table->string('name',250);
            $table->string('picture',250);
            $table->string('barcode',250)->nullable()->default(null);
            $table->string('barcode2',250)->nullable()->default(null);
            $table->string('barcode3',250)->nullable()->default(null);
            $table->enum('on_deal',['YES','NO'])->default('YES');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_shades');
    }
}
