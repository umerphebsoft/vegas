<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockPercentagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_percentages', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->date('adate')->nullable()->default(null);
            $table->string('total_products',250)->nullable()->default(null);
            $table->string('available',250)->nullable()->default(null);
            $table->string('out_of_stock',250)->nullable()->default(null);
            $table->string('percentage',250)->nullable()->default(null);
            $table->string('total_407',250)->nullable()->default(null);
            $table->string('total_e11',250)->nullable()->default(null);
            $table->string('percentage_e11',250)->nullable()->default(null);
            $table->string('total_safa',250)->nullable()->default(null);
            $table->string('percentage_safa',250)->nullable()->default(null);
            $table->string('total_bahria',250)->nullable()->default(null);
            $table->string('percentage_bahria',250)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_percentages');
    }
}
