<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audit_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('audit_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id')->nullable();
            $table->unsignedInteger('previous_assigned_qty')->nullable();
            $table->unsignedInteger('sold_qty')->nullable();;
            $table->unsignedInteger('available_qty')->nullable();
            $table->date('adate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audit_details');
    }
}
