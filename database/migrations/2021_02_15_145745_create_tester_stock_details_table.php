<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTesterStockDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tester_stock_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            // $table->string('barcode',250)->nullable()->default(null);
            // $table->string('pname',250)->nullable()->default(null);
            // $table->string('sname',250)->nullable()->default(null);
            // $table->string('zname',250)->nullable()->default(null);
            // $table->unsignedInteger('id')
            // $table->unsignedInteger('id')
            // $table->unsignedInteger('id')
            // $table->unsignedInteger('id')
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tester_stock_details');
    }
}
