<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreordersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preorders', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('product_name',255);
            $table->string('brand_name',255);
            $table->string('shade_name',255);
            $table->integer('qty');
            $table->string('name',255);
            $table->string('email',255);
            $table->string('telephone',255);
            $table->string('address',255);
            $table->string('city',255);
            $table->dateTime('o_date');
            $table->enum('status',['YES','NO']);
            $table->string('couponcode',255)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preorders');
    }
}
