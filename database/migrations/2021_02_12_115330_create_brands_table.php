<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('leed_time');
            $table->string('brand_slug',250);
            $table->string('en_title',250);
            $table->string('en_meta_description',250);
            $table->string('en_keywords',250);
            $table->string('en_menu_title',250);
            $table->string('en_heading',250);
            $table->text('en_details');
            $table->string('banner',250);
            $table->string('banner2',250)->nullable();
            $table->string('picture',250);
            $table->date('adate');
            $table->integer('discount')->nullable();
            $table->enum('status',['NO','YES'])->nullable();
            $table->integer('view_order')->nullable();
            $table->integer('featured')->nullable();
            $table->string('featuredimage',250)->nullable();
            $table->string('downloadfile',250)->nullable();
            $table->string('margin',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brands');
    }
}

