<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_stocks', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('store_id');
            $table->unsignedInteger('assign_by');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id')->nullable()->default(0);
            $table->unsignedInteger('size_id')->nullable()->default(0);
            $table->integer('previous');
            $table->integer('sold');
            $table->integer('available');
            $table->integer('qty');
            $table->date('pdate');
            $table->enum('status',['YES','NO'])->nullable()->default('YES');
            $table->unsignedInteger('purchase_supply_id')->nullable()->default(null);
            $table->integer('audited')->nullable()->default(null);
            $table->dateTime('audit_date')->nullable()->default(null);
            $table->unsignedInteger('tester_created_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_stocks');
    }
}
