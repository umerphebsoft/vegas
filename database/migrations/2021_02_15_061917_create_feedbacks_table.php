<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedbacks', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('name',255);
            $table->string('email',255);
            $table->string('telephone',255);
            $table->string('order_no',255);
            $table->text('details');
            $table->text('pictures');
            $table->dateTime('f_date');
            $table->enum('status',['YES','NO'])->nullable()->default('NO');
            $table->string('reason',255)->nullable()->default(null);
            $table->text('comment')->nullable()->default(null);
            $table->string('reply_by',250)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedbacks');
    }
}
