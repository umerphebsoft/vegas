<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('cat_slug',250);
            $table->string('en_title',250);
            $table->string('en_meta_description',250);
            $table->string('en_keywords',250);
            $table->string('en_menu_title',250);
            $table->string('en_heading',250);
            $table->text('en_details');
            $table->string('banner',250)->nullable();
            $table->string('banner2',250)->nullable();
            $table->string('picture',250)->nullable();
            $table->date('adate')->nullable();
            $table->enum('status',['YES','NO'])->nullable();
            $table->integer('view_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
