<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_order_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('purchase_order_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id');
            $table->unsignedInteger('size_id');
            $table->integer('qty');
            $table->date('adate');
            $table->enum('edited',['YES','NO'])->default('NO');
            $table->string('mrp',250)->nullable()->default(null);
            $table->string('tax',250)->nullable()->default(null);
            $table->string('tpwotax',250)->nullable()->default(null);
            $table->string('tpwtax',250)->nullable()->default(null);
            $table->string('margin',250)->nullable()->default(null);
            $table->string('tprice',250)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_order_details');
    }
}
