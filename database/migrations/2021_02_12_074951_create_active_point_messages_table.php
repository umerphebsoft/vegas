<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivePointMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('active_point_messages', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->integer('user_id');
            $table->string('name', 255);
            $table->string('phone', 255);
            $table->date('sysdate');
            $table->float('points', 13, 2);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('active_point_messages');
    }
}
