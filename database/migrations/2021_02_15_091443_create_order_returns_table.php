<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_returns', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('user_id')->nullable()->default(0);
            $table->string('order_no',250);
            $table->string('fname',250);
            $table->string('lname',250);
            $table->string('email',250);
            $table->string('phone',250);
            $table->string('address',250);
            $table->string('city',250);
            $table->string('country',250);
            $table->string('country_code',250);
            $table->string('postal_code',250);
            $table->string('state',250);
            $table->enum('status',['YES','NO']);
            $table->integer('cancel')->nullable()->default(0);
            $table->string('shipping_charges',250);
            $table->unsignedInteger('coupon_id')->nullable()->default(null);
            $table->float('discount',13,2)->nullable()->default(null);
            $table->float('reward_used',13,2)->nullable()->default(null);
            $table->string('total_price',250)->nullable()->default(null);
            $table->dateTime('order_date')->nullable()->default(null);
            $table->string('ip_address',250)->nullable()->default(null);
            $table->string('extra_info',250)->nullable()->default(null);
            $table->integer('deleted')->nullable()->default(0);
            $table->integer('review_sent')->nullable()->default(null);
            $table->integer('confirm')->nullable()->default(null);
            $table->unsignedInteger('courier_id')->nullable()->default(null);
            $table->string('tracking_no',250)->nullable()->default(null);
            $table->integer('delivered')->nullable()->default(0);
            $table->integer('paid')->nullable()->default(0);
            $table->float('paid_amount',13,2)->nullable()->default(null);
            $table->date('paid_date',13,2)->nullable()->default(null);
            $table->string('payment_mode',250)->nullable()->default(null);
            $table->integer('forrun')->nullable()->default(null);
            $table->integer('bankreceived')->nullable()->default(null);
            $table->integer('payment_difference')->nullable()->default(null);
            $table->string('deletedby',250)->nullable()->default(null);
            $table->string('comments',250)->nullable()->default(null);
            $table->integer('webonly')->nullable()->default(0);
            $table->unsignedInteger('store_id')->default(0);
            $table->float('store_cash',13,2)->nullable()->default(0);
            $table->float('store_balance',13,2)->nullable()->default(0);
            $table->integer('out_of_stock')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_returns');
    }
}
