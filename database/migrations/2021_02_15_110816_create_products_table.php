<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('cat_id');
            $table->unsignedInteger('sub_cat_id');
            $table->unsignedInteger('brand_id');
            $table->enum('multi_colors',['YES','NO'])->default('NO');
            $table->enum('multi_sizes',['YES','NO'])->nullable()->default('NO');
            $table->string('color_ids',250);
            $table->string('url_slug',250);
            $table->string('en_title',250);
            $table->string('en_meta_description',250);
            $table->string('en_keywords',250);
            $table->string('en_menu_title',250);
            $table->string('en_heading',250);
            $table->string('en_short_detail',250);
            $table->text('en_details');
            $table->string('youtube',250);
            $table->string('pictures',250);
            $table->double('price',10,0);
            $table->double('discount_price',10,0);
            $table->enum('on_deal',['YES','NO'])->default('NO');
            $table->integer('total_sold')->nullable()->default(0);
            $table->integer('eid_sale')->default(0);
            $table->date('adate');
            $table->enum('status',['YES','NO']);
            $table->integer('view_order')->default(0);
            $table->integer('old_price')->default(0);
            $table->integer('old_discount_price')->default(0);
            $table->integer('reorder_level');
            $table->enum('store_only',['YES','NO'])->default('NO');
            $table->enum('web_only',['YES','NO'])->default('NO');
            $table->enum('on_sale',['YES','NO'])->default('NO');
            $table->integer('discount')->default(0);
            $table->enum('coming_soon',['YES','NO'])->default('NO');
            $table->string('barcode',250)->default(null);
            $table->string('barcode2',250)->default(null);
            $table->string('barcode3',250)->default(null);
            $table->float('tp')->nullable()->default(0);
            $table->float('gst')->nullable()->default(0);
            $table->enum('free_product',['YES','NO'])->default('NO');
            $table->integer('available_items')->nullable()->default(null);
            $table->integer('total_items')->nullable()->default(null);
            $table->unsignedInteger('supplier_id')->nullable()->default(null);
            $table->integer('new_product')->nullable()->default(0);
            $table->string('packshot',250)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
