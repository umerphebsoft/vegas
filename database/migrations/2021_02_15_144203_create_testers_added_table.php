<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestersAddedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testers_added', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('supply_no',250);
            $table->unsignedInteger('store_id')->nullable()->default(null);
            $table->dateTime('supply_date')->nullable()->default(null);
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->default(0);
            $table->enum('product_added',['YES','NO'])->default('NO');
            $table->integer('type')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testers_added');
    }
}
