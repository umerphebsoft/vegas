<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierPrintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courier_prints', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('order_id');
            $table->string('order_no',250);
            $table->unsignedInteger('courier_id');
            $table->string('courier_name',250);
            $table->string('tracking_no',250);
            $table->string('name',250);
            $table->string('phone',250);
            $table->string('city',250);
            $table->float('amount',13,2);
            $table->float('sysdate');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier_prints');
    }
}
