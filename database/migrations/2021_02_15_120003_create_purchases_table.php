<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('purchase_no',250);
            $table->string('invoice_no',250)->nullable()->default(null);
            $table->date('invoice_date')->nullable()->default(null);
            $table->unsignedInteger('supplier_id')->nullable()->default(null);
            $table->string('purchase_order_no',250)->nullable()->default(null);
            $table->string('payment_method',250)->nullable()->default(null);
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->nullable()->default(0);
            $table->enum('product_added',['YES','NO'])->nullable()->default('NO');
            $table->float('amount',13,2)->nullable()->default(0);
            $table->float('discount',13,2)->nullable()->default(0);
            $table->float('total',13,2)->nullable()->default(0);
            $table->float('tax',13,2)->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchases');
    }
}
