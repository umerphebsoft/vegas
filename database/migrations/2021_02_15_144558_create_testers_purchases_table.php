<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestersPurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('testers_purchases', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('purchase_no',250);
            $table->string('invoice_no',250);
            $table->date('invoice_date');
            $table->unsignedInteger('supplier_id');
            $table->dateTime('purchase_date');
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->default(0);
            $table->enum('product_added',['YES','NO'])->default('NO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('testers_purchases');
    }
}
