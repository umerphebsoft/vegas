<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierPayablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courier_payables', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('order_id');
            $table->dateTime('assigned_date');
            $table->string('order_total_price',250)->nullable()->default(null);
            $table->string('courier_rate',250)->nullable()->default(null);
            $table->string('total_payable',250)->nullable()->default(null);
            $table->enum('status',['YES','NO'])->nullable()->default('YES');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier_payables');
    }
}
