<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('project_name_at_backend',250);
            $table->string('address',250);
            $table->string('phone',250);
            $table->string('mobile',250);
            $table->string('email',250);
            $table->string('fax',250);
            $table->string('paypal_email',250);
            $table->string('sitemap',250);
            $table->string('robots',250);
            $table->string('webmaster',250);
            $table->string('brand_title',250);
            $table->string('brand_keywords',250);
            $table->string('brand_description',250);
            $table->string('product_title',250);
            $table->string('product_keywords',250);
            $table->string('product_description',250);
            $table->string('order_message',250);
            $table->string('review_message',250);
            $table->string('signup_message',250);
            $table->string('invite_message',250);
            $table->text('points_reminder');
            $table->string('expiry_message',250);
            $table->string('shipping_message',250);
            $table->string('modified_message',250);
            $table->string('addextra_message',250);
            $table->string('deleted_message',250);
            $table->float('shipping_local',13,2);
            $table->float('shipping',13,2);
            $table->string('from_email',250);
            $table->string('blogger_message',250);
            $table->text('top_text');
            $table->text('voucher_message')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
