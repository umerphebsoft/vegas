<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_links', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('agent_id');
            $table->string('facebook_link',250);
            $table->string('twitter_link',250);
            $table->string('linkedin_link',250);
            $table->string('google_plus',250);
            $table->string('pinterest',250);
            $table->string('instagram',250);
            $table->string('youtube',250);
            $table->string('snapchat',250);
            $table->date('date');
            $table->enum('status',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('social_links');
    }
}
