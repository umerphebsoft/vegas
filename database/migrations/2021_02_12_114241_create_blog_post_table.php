<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('cat_id');
            $table->string('blog_slug',250);
            $table->string('en_title',250);
            $table->string('en_meta_description',250);
            $table->string('en_keywords',250);
            $table->string('en_menu_title',250);
            $table->string('heading',250);
            $table->text('details');
            $table->string('picture',250);
            $table->date('adate');
            $table->enum('status',['YES','NO']);
            $table->integer('view_order');
            $table->integer('views');
            $table->string('youtube',250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post');
    }
}
