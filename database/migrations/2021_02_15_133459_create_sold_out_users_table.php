<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoldOutUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sold_out_users', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('name',250);
            $table->string('phone',250);
            $table->string('email',250);
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('color_id');
            $table->date('date');
            $table->enum('status',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sold_out_users');
    }
}
