<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseAlertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_alerts', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('product_id');
            $table->string('name',250);
            $table->unsignedInteger('color_id');
            $table->string('shade',250);
            $table->date('leed_date');
            $table->time('leed_time');
            $table->float('quantity_sale',13,2);
            $table->float('quantity_left',13,2);
            $table->float('daily_sale',13,2);
            $table->float('monthly_sale',13,2);
            $table->date('stock_finish_date')->nullable()->default(null);
            $table->enum('status',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_alerts');
    }
}
