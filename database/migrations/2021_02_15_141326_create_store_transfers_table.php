<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_transfers', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('transfer_no',250);
            $table->unsignedInteger('transfer_from_store_id');
            $table->unsignedInteger('transfer_to_store_id');
            $table->unsignedInteger('brand_id');
            $table->dateTime('transfer_date');
            $table->dateTime('receive_date');
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->default(0);
            $table->enum('product_added',['YES','NO'])->default('NO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_transfers');
    }
}
