<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplies', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('supply_no',250);
            $table->unsignedInteger('store_id');
            $table->unsignedInteger('brand_id')->nullable()->default(null);
            $table->date('supply_date')->nullable()->default(null);
            $table->date('receive_date')->nullable()->default(null);
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->default(0);
            $table->enum('product_added',['YES','NO'])->default('NO');
            $table->integer('type')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplies');
    }
}
