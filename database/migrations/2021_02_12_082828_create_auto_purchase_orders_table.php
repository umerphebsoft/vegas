<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoPurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_purchase_orders', function (Blueprint $table) {
           $table->unsignedInteger('id')->autoIncrement();
           $table->string('purchase_order_no',250);
           $table->unsignedInteger('supplier_id');
           $table->unsignedInteger('brand_id');
           $table->dateTime('purchase_order_date');
           $table->enum('status',['YES','NO']);
           $table->boolean('deleted')->default(0);
           $table->enum('product_added',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_purchase_orders');
    }
}
