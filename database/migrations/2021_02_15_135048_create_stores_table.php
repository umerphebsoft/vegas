<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('store_name',250);
            $table->string('address',250);
            $table->string('email',250);
            $table->string('phone',20);
            $table->date('join_date');
            $table->string('ip_address',250);
            $table->string('latitude',250)->nullable()->default(null);
            $table->string('longitude',250)->nullable()->default(null);
            $table->enum('status',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
