<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoldBestProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sold_best_products', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('name',250);
            $table->string('shade_name',250);
            $table->string('brand',250);
            $table->float('price',13,2);
            $table->float('discount_price',13,2);
            $table->integer('qty');
            $table->integer('sold');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sold_best_products');
    }
}
