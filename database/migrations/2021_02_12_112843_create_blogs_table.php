<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('blog_slug',250);
            $table->string('en_title',250);
            $table->string('en_meta_description',250);
            $table->string('en_keywords',250);
            $table->string('en_menu_title',250);
            $table->string('en_heading',250);
            $table->text('en_details');
            $table->string('picture',250);
            $table->date('adate');
            $table->enum('status',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
