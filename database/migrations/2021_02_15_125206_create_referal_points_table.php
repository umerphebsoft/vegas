<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferalPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referal_points', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->integer('registration_points');
            $table->float('shopping_points',13,2);
            $table->integer('referal_registration_points');
            $table->float('referal_shopping_points',13,2);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referal_points');
    }
}
