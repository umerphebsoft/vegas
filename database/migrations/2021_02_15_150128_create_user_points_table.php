<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserPointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_points', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->unsignedInteger('referal_user_id')->nullable()->default(null);
            $table->string('title',250)->nullable()->default(null);
            $table->integer('points')->nullable()->default(null);
            $table->integer('used')->nullable()->default(null);
            $table->unsignedInteger('order_id')->nullable()->default(null);
            $table->date('sysdate')->nullable()->default(null);
            $table->date('expdate')->nullable()->default(null);
            $table->integer('expsent')->nullable()->default(null);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_points');
    }
}
