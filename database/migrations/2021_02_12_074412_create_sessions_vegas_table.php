<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionsVegasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sessions_vegas', function (Blueprint $table) {
            $table->string('session_id', 50)->primary();
            $table->string('ip_address', 50);
            $table->string('user_agent', 50);
            $table->integer('last_activity');
            $table->text('user_data');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sessions_vegas');
    }
}
