<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('coupon',250);
            $table->unsignedInteger('discount');
            $table->unsignedInteger('brand_id')->nullable();
            $table->unsignedInteger('blogger');
            $table->unsignedInteger('status');
            $table->enum('coupon_type',['Brand','Amount','Percentage','Product'])->default('Percentage');
            $table->unsignedInteger('min_price')->nullable();
            $table->unsignedInteger('no_of_times')->nullable();
            $table->boolean('deleted')->nullable()->default(0);
            $table->string('name',250)->nullable();
            $table->string('cell',250)->nullable();
            $table->string('order_no',250)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
