<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('bank_name',250)->nullable();
            $table->string('account_no',250)->nullable();
            $table->enum('status',['YES','NO'])->nullable();
            $table->string('title',250)->nullable();
            $table->string('address',250)->nullable();
            $table->string('swiftcode',250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
