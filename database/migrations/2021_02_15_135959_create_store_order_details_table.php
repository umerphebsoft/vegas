<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_order_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('store_id');
            $table->string('pname',250);
            $table->unsignedInteger('color_id');
            $table->unsignedInteger('size_id')->default(0);
            $table->string('quantity',250);
            $table->enum('pickup_status',['YES','NO'])->nullable()->default('NO');
            $table->date('pickup_date')->nullable()->default(null);
            $table->dateTime('order_date')->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_order_details');
    }
}
