<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_emails', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('name',250)->nullable()->default(null);
            $table->string('email',250);
            $table->unsignedInteger('product_id')->nullable()->default(null);
            $table->string('p_name',250)->nullable()->default(null);
            $table->unsignedInteger('color_id')->nullable()->default(null);
            $table->string('img',250)->nullable()->default(null);
            $table->date('sysdate');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_emails');
    }
}
