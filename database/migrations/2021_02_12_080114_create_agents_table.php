<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->integer('type');
            $table->string('user_name',100);
            $table->string('cname',100);
            $table->string('password',250);
            $table->string('email',100);
            $table->string('address',150);
            $table->string('postcode',20);
            $table->string('cell',16);
            $table->date('joinDate');
            $table->dateTime('lastLogin');
            $table->string('ipAddress',26);
            $table->enum('status',['NO','YES']);
            $table->string('picture',100);
            $table->enum('featured',['NO','YES']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}

