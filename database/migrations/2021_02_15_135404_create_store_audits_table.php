<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_audits', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('store_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id')->default(0);
            $table->unsignedInteger('size_id');
            $table->integer('pavailable');
            $table->integer('porders');
            $table->integer('qty');
            $table->date('pdate');
            $table->enum('status',['YES','NO'])->default('YES');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_audits');
    }
}
