<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBykeaOrderTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bykea_order_tracking', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('event',250)->default('NULL');
            $table->string('event_id',250)->default('NULL');
            $table->string('event_time',250)->default('NULL');
            $table->string('tracking_id',250)->default('NULL');
            $table->string('tracking_link',250)->default('NULL');
            $table->string('pname',250)->default('NULL');
            $table->string('pmobile',250)->default('NULL');
            $table->string('plate_no',250)->default('NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bykea_order_tracking');
    }
}
