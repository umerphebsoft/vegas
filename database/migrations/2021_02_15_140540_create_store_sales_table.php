<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_sales', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('store_id');
            $table->integer('total_amount');
            $table->integer('total_orders');
            $table->integer('cash_amount');
            $table->integer('cash_orders');
            $table->integer('card_amount');
            $table->integer('card_orders');
            $table->integer('return_amount')->default(0);
            $table->date('sale_date');
            $table->string('sale_month',15);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_sales');
    }
}
