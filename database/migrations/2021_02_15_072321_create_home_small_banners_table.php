<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHomeSmallBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home_small_banners', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('brand_id')->nullable()->default(null);
            $table->integer('position')->nullable()->default(null);
            $table->string('url',250)->nullable()->default(null);
            $table->string('image',250)->nullable()->default(null);
            $table->dateTime('adate')->nullable()->default(null);
            $table->enum('status',['YES','NO'])->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('home_small_banners');
    }
}
