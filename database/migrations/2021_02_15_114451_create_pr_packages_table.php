<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePrPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pr_packages', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('name',250);
            $table->string('phone',250);
            $table->string('email',250);
            $table->string('city',250);
            $table->string('address',250);
            $table->string('blog_link',250)->nullable()->default(null);
            $table->integer('blog_followers')->nullable()->default(0);
            $table->string('instagram_link',250)->nullable()->default(null);
            $table->integer('instagram_followers')->nullable()->default(0);
            $table->string('twitter_link',250)->nullable()->default(null);
            $table->integer('twitter_followers')->nullable()->default(0);
            $table->string('facebook_link',250)->nullable()->default(null);
            $table->integer('facebook_followers')->nullable()->default(0);
            $table->string('youtube_link',250)->nullable()->default(null);
            $table->integer('youtube_followers')->nullable()->default(0);
            $table->string('snapchat_link',250)->nullable()->default(null);
            $table->integer('snapchat_followers')->nullable()->default(0);
            $table->date('sysdate');
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_packages');
    }
}
