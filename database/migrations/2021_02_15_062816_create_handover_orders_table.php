<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHandoverOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('handover_orders', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('reference_no',255);
            $table->unsignedInteger('courier_id');
            $table->string('total_orders',255);
            $table->string('total_amount',255);
            $table->dateTime('ho_date');
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->default(0);
            $table->enum('product_added',['YES','NO'])->default('NO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('handover_orders');
    }
}
