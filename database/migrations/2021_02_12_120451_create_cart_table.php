<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('session_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('cat_id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('color_id')->default(0);
            $table->unsignedInteger('size_id')->default(0);
            $table->unsignedInteger('qty');
            $table->float('price',13,2);
            $table->string('name',250);
            $table->date('sysdate');
            $table->string('time',250);
            $table->integer('status');
            $table->float('o_price',13,2);
            $table->string('s1_407',250)->default('NULL');
            $table->string('s2_e11',250)->default('NULL');
            $table->string('s3_safa',250)->default('NULL');
            $table->string('s4_bahria',250)->default('NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
