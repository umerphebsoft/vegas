<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMyInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('my_inventory', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('shade_id');
            $table->string('shade',250);
            $table->string('size_name',250)->nullable()->default(null);
            $table->unsignedInteger('product_id');
            $table->string('product',250);
            $table->string('barcode',250)->nullable()->default(null);
            $table->unsignedInteger('brand_id');
            $table->string('brand',250);
            $table->float('price',13,2);
            $table->float('discount_price',13,2);
            $table->integer('total');
            $table->integer('sold');
            $table->integer('supply')->nullable()->default(null);
            $table->integer('balance')->nullable()->default(0);
            $table->float('avg_sale',13,2)->nullable()->default(0);
            $table->integer('e11')->nullable()->default(0);
            $table->integer('bahria')->nullable()->default(0);
            $table->integer('safa')->nullable()->default(0);
            $table->integer('warehouse303')->nullable()->default(0);
            $table->enum('webonly',['YES','NO'])->nullable()->default(null);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('my_inventory');
    }
}

