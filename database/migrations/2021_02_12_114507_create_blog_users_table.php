<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_users', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('fname',100);
            $table->string('password',250);
            $table->string('email',100);
            $table->string('phone',150);
            $table->string('facebook',150);
            $table->string('twitter',150);
            $table->string('instagram',150);
            $table->string('website',150);
            $table->string('about',255);
            $table->date('join_date');
            $table->dateTime('last_login');
            $table->string('ip_address',26);
            $table->enum('status',['NO','YES']);
            $table->string('picture',100);
            $table->enum('featured',['NO','YES']);
            $table->integer('login');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_users');
    }
}
