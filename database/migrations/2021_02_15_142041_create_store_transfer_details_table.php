<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreTransferDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_transfer_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('transfer_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id')->default(0);
            $table->unsignedInteger('size_id');
            $table->integer('qty');
            $table->date('adate');
            $table->enum('edited',['YES','NO'])->default('NO');
            $table->string('trade_price',250)->nullable()->default(null);
            $table->string('gst',250)->nullable()->default(null);
            $table->string('sale_price',250)->nullable()->default(null);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_transfer_details');
    }
}
