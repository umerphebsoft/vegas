<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTesterAutoSuppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tester_auto_supplies', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('supply_no',250);
            $table->unsignedInteger('store_id');
            $table->unsignedInteger('brand_id');
            $table->dateTime('supply_date');
            $table->dateTime('receive_date');
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->default(0);
            $table->enum('product_added',['YES','NO'])->default('NO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tester_auto_supplies');
    }
}
