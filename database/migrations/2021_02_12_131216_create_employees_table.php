<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('store_id');
            $table->string('name',250)->nullable()->default(null);
            $table->string('password',250)->nullable()->default(null);
            $table->string('salary',250)->nullable()->default(null);
            $table->string('email',250)->nullable()->default(null);
            $table->string('cell',250)->nullable()->default(null);
            $table->date('join_date')->nullable()->default(null);
            $table->date('last_login')->nullable()->default(null);
            $table->string('ip_address',250)->nullable()->default(null);
            $table->enum('status',['YES','NO'])->nullable()->default('YES');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
