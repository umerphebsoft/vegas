<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAutoSuppliesDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auto_supplies_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('supply_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id')->default(0);
            $table->unsignedInteger('size_id');
            $table->unsignedInteger('qty');
            $table->dateTime('adate');
            $table->enum('edited',['YES','NO'])->default('NO');
            $table->boolean('deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_supplie_details');
    }
}
