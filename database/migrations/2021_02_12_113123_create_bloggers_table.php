<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloggers', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->integer('type');
            $table->string('user_name',250);
            $table->string('cname',250);
            $table->string('password',250);
            $table->string('email',250);
            $table->string('address',250);
            $table->string('postcode',250);
            $table->string('cell',250);
            $table->unsignedInteger('coupon_id');
            $table->string('coupon',250);
            $table->integer('discount');
            $table->integer('myprofit');
            $table->date('join_date');
            $table->dateTime('last_login');
            $table->string('ip_adress',250);
            $table->unsignedInteger('product_id')->default(0);
            $table->unsignedInteger('brand_id')->default(0);
            $table->enum('status',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloggers');
    }
}
