<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDamageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('damage_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('damage_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id');
            $table->unsignedInteger('size_id');
            $table->string('ord_no',250)->nullable()->default(null);
            $table->string('c_no',250)->nullable()->default(null);
            $table->integer('qty');
            $table->string('unit_price',250)->nullable()->default(null);
            $table->string('net_amount',250)->nullable()->default(null);
            $table->date('pdate');
            $table->enum('status',['YES','NO'])->default('YES');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('damage_details');
    }
}
