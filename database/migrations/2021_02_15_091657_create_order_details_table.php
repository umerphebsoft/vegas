<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('product_id');
            $table->string('pname',255);
            $table->unsignedInteger('color_id');
            $table->unsignedInteger('size_id')->default(0);
            $table->string('quantity',255);
            $table->string('unit_price',255);
            $table->string('net_price',255);
            $table->integer('store_return')->default(0);
            $table->date('return_date')->nullable()->default(null);
            $table->string('s1_407',255)->nullable()->default(null);
            $table->string('s2_e11',255)->nullable()->default(null);
            $table->string('s3_safa',255)->nullable()->default(null);
            $table->string('s4_bahria',255)->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
