<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pre_order', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('order_id');
            $table->string('fname',250);
            $table->string('lname',250);
            $table->string('email',250);
            $table->string('phone',250);
            $table->string('address',250);
            $table->string('city',250);
            $table->string('country',250);
            $table->string('country_code',250);
            $table->string('postal_code',250);
            $table->string('state',250);
            $table->enum('status',['YES','NO']);
            $table->string('shipping_charges',250);
            $table->string('total_price',250);
            $table->date('order_date');
            $table->string('ip_address',250);
            $table->string('extra_info',250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pre_order');
    }
}
