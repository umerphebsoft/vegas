<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreWebsiteOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_website_orders', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('reference_no',250)->nullable()->default(null);
            $table->unsignedInteger('store_id');
            $table->string('total_products',250)->nullable()->default(null);
            $table->date('created_date')->nullable()->default(null);
            $table->date('receive_date')->nullable()->default(null);
            $table->enum('status',['YES','NO'])->default('NO');
            $table->integer('deleted')->default(0);
            $table->enum('product_added',['YES','NO'])->default('NO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_website_orders');
    }
}
