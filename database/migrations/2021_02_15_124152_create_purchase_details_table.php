<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_details', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('purchase_id');
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('shade_id');
            $table->unsignedInteger('size_id');
            $table->integer('qty');
            $table->dateTime('adate');
            $table->enum('status',['YES','NO'])->default('NO');
            $table->string('trade_price',250)->default(null);
            $table->string('gst',250)->default(null);
            $table->string('sale_price',250)->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_details');
    }
}
