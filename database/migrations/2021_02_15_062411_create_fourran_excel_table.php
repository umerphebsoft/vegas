<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFourranExcelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fourran_excel', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('pick_from_name',255);
            $table->string('pick_from_num',255);
            $table->string('pick_from_add',255);
            $table->string('pick_from_city',255);
            $table->string('del_to_name',255);
            $table->string('del_from_num',255);
            $table->string('del_from_add',255);
            $table->string('del_from_city',255);
            $table->string('amount',255);
            $table->string('notes',255);
            $table->string('item_detail',255);
            $table->float('weight',13,2);
            $table->integer('length');
            $table->integer('width');
            $table->integer('height');
            $table->string('reference_no',255);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fourran_excel');
    }
}

