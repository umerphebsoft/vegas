<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVtoRenderingDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vto_rendering_data', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('brand')->nullable()->default(null);
            $table->string('category')->nullable()->default(null);
            $table->string('barcode')->nullable()->default(null);
            $table->string('tune')->nullable()->default(null);
            $table->string('color_atune')->nullable()->default(null);
            $table->string('intensity')->nullable()->default(null);
            $table->string('gamma')->nullable()->default(null);
            $table->string('sparkle_a')->nullable()->default(null);
            $table->string('sparkle_r')->nullable()->default(null);
            $table->string('sparkle_g')->nullable()->default(null);
            $table->string('sparkle_b')->nullable()->default(null);
            $table->string('color_r')->nullable()->default(null);
            $table->string('color_g')->nullable()->default(null);
            $table->string('color_b')->nullable()->default(null);
            $table->string('placement')->nullable()->default(null);
            $table->string('sparkle_density')->nullable()->default(null);
            $table->string('sparkle_size')->nullable()->default(null);
            $table->string('sparkle_color_variation')->nullable()->default(null);
            $table->string('sparkle_base_reflectivity')->nullable()->default(null);
            $table->string('skin_glow')->nullable()->default(null);
            $table->string('metallic_intensity')->nullable()->default(null);
            $table->string('family')->nullable()->default(null);
            $table->string('finish')->nullable()->default(null);
            $table->string('skin_clearing')->nullable()->default(null);
            $table->string('contrast_boost')->nullable()->default(null);
            $table->string('lip_plumping')->nullable()->default(null);
            $table->string('env_mapping_intensity')->nullable()->default(null);
            $table->string('env_mapping_r')->nullable()->default(null);
            $table->string('env_mapping_g')->nullable()->default(null);
            $table->string('env_mapping_b')->nullable()->default(null);
            $table->string('wetness')->nullable()->default(null);
            $table->string('env_mapping_bump_intensity')->nullable()->default(null);
            $table->string('env_mapping_curve')->nullable()->default(null);
            $table->string('env_mapping_rotation_y')->nullable()->default(null);
            $table->string('preset_style')->nullable()->default(null);
            $table->string('gloss_detail')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vto_rendering_data');
    }
}
