<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->unsignedInteger('agent_id');
            $table->string('page_name',250);
            $table->string('page_type',250);
            $table->string('title',250);
            $table->enum('type',['Aboutus','Academics','Admissions','general','Roll','Alumni','Result'])->default('general');
            $table->string('meta_tags',250);
            $table->string('keywords',250);
            $table->string('menu_title',250);
            $table->string('heading',250);
            $table->text('details');
            $table->string('banner',250);
            $table->string('banner2',250);
            $table->date('date');
            $table->enum('status',['YES','NO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
