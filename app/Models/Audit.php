<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    use HasFactory;

    protected $table = "audits";

    protected $fillable = [
      'audit_no','store_id','audit_date','status'
    ];

}
