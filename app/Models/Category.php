<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = "categories";

    protected $fillable = [
      'cat_slug','en_title','en_meta_description','en_keywords','en_heading','en_menu_title','en_details','banner','banner2','picture','adate','status',
      'view_order'
    ];

}
