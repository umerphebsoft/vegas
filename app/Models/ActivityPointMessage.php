<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ActivityPointMessage extends Model
{
    use HasFactory;

    protected $table = "active_point_messages";

    protected $fillable = [
      'user_id','name','phone','sysdate','points','status',
    ];

}
