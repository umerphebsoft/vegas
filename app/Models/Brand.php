<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $table = "brands";

    protected $fillable = [
      'leed_time','brand_slug','en_title','en_meta_description','en_keywords','en_heading','en_menu_title','en_details','banner','banner2','picture','adate','discount','status',
      'view_order','featured','featuredimage','downloadfile','margin'
    ];

}
