<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $table = "blogs";

    protected $fillable = [
      'blog_slug','en_title','en_meta_description','en_keywords','en_heading','en_menu_title','en_details','picture','adate','status'
    ];

}
