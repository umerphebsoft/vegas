<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    use HasFactory;

    protected $table = "social_links";

    protected $fillable = [
      'agent_id','facebook_link','twitter_link','linkedin_link','google_plus','pinterest','instagram','youtube','snapchat','date','status'
    ];
}
