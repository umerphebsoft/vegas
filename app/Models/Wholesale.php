<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wholesale extends Model
{
    use HasFactory;

    protected $table = "wholesales";

    protected $fillable = [
      'name','telephone','company_name','email','city','message','contact_date'
    ];

}
